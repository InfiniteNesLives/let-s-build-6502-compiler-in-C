#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cradle.h"

void Term();
void Expression();
void Add();
void Substract();
void Factor();
void Ident();
void Assignment();


//unchanged since last installment
//Let's cleanup for last time to focus on 6502
//6502 relies on sparately defined subroutine for multiply
void Multiply()
//multiplicand is in A, multiplier is on stack
//leave sum in A
{
    Match('*');
    Factor();

    //multiplicand is in Accumulator
    EmitLn("STA multiplicand");
    //multiplier is on the stack
    EmitLn("PLA");
    EmitLn("STA multiplier");
    EmitLn("JSR multiply");
    //Sum is in Accumlator upon return
} 

//unchanged since last installment
//Let's cleanup for last time to focus on 6502
//6502 relies on sparately defined subroutine for division
void Divide()
//denominator is in A, numerator is on stack
//leave quotient in A
{
    Match('/');
    Factor();

    //denominator is in Accumulator
    EmitLn("STA denominator");
    //numerator is on the stack
    EmitLn("PLA");
    EmitLn("STA numerator");
    EmitLn("JSR divide");
    //quotient is in the Accumulator

}

//new function for this installment
//Used to differentiate between variables and functions
//by the presence of parenth following char/string
/*
 * Jack gets the name of the var/func and then determines
 * if a function based on presence of parenths
 * If function call subroutine with that name
 * Else it's a variable and gets loaded into D0
{---------------------------------------------------------------}
{ Parse and Translate a Identifier }

procedure Ident;
var Name: string[8];
begin
   Name:= GetName;
   if Look = '(' then begin
      Match('(');
      Match(')');
      EmitLn('BSR ' + Name);
      end
   else
      EmitLn('MOVE ' + Name + '(PC),D0');
end;
*/
void Ident()
{
    char *name = GetName();
    if (Look == '(') {
        Match('(');
        Match(')');
        //x86: sprintf(tmp, "call %s", name);
        sprintf(tmp, "JSR %s", name);
        EmitLn(tmp);
    } else {
        //x86: sprintf(tmp, "movl %s, %%eax", name);
        sprintf(tmp, "LDA %s", name);
        EmitLn(tmp);
    }
}

//factor gets minor update this installment
//supports strings for getnum and calls identify if an alpha
//x86 version handles the leading - sign for negative values
//but Jack gets to this eventually anyway and it looks like these
//x86 guys forgot to cut the handling from expression function..
void Factor()
//mathematic definition of factor is the separate parts of a mult/divide
//shouldn't be any internal add/subtract terms unless there's parethesis
//in which case we just consider the contents a separate expression
//This function collects a factor and loads to Accumulator
//if it turns out to be a function instead of a factor idenifty will resolve
//end result is the factor is left in the Accumulator or the function gets called
{
    if(Look == '(') {
	//if pareth, treat as separate expression
        Match('(');
        Expression();
        Match(')');
	/* x86 handling leading - sign
     } else if(IsAddop(Look)) {
        Match('-');
        sprintf(tmp,"movl $%s, %%eax", GetNum());
        EmitLn(tmp);
        EmitLn("negl %eax");
	*/
    } else if (IsAlpha(Look)) {
	//determine if variable or function and handle accordingly
        Ident();
    } else {
	//load constant
        //hex leading $ or trailing h: sprintf(tmp,"LDA #$%s", GetNum());
	//decimal is bare number in ca65
        sprintf(tmp,"LDA #%s", GetNum());
        EmitLn(tmp);
    }
}

//unchanged since last installment
//Let's cleanup for last time to focus on 6502
void Term()
//mathematic definition of a term are the parts of an expression separated by +/-
//terms can be made up of multiple factors so we need to process each factor
//of the current term
//end result is the current term is left in the Accumulator
{
    //collect first factor and place in Accumulator
    Factor();
    while (strchr("*/", Look)) {

        //first factor is in Accumulator 
	//push to stack prior to calling mult/div
        EmitLn("PHA");

        switch(Look)
        {
            case '*':
                Multiply();
                break;
            case '/':
                Divide();
                break;
            default:
                Expected("Mulop");
        }
    }
}

//unchanged since last installment
//Let's cleanup for last time to focus on 6502
void Expression()
//evaluate expression and leave result in Accumulator
{
    if(IsAddop(Look))
    //starts with +/- sign treat as 0+term or 0-term
        EmitLn("LDA #$00");
    else
        Term();

    while (strchr("+-", Look)) {

	//push current term on stack
        EmitLn("PHA");

        switch(Look)
        {
            case '+':
                Add();
                break;
            case '-':
                Substract();
                break;
            default:
                Expected("Addop");
        }
    }
}


//unchanged since last installment
//Let's cleanup for last time to focus on 6502
void Add()
//add the value on the stack to the Accumulator
//leave sum in the Accumulator
{
    Match('+');
    Term();

    //first store accumulator to a byte in zero page
    EmitLn("STA $00");
    //pop from stack to accum
    EmitLn("PLA");
    //clear carry prior to add otherwise extra bit will get added
    //effectively Accum = Accum + mem + carry
    EmitLn("CLC");
    //Add carry, Accumulator, and Zero Page byte
    EmitLn("ADC $00");
    //Sum is in accumulator
}


//unchanged since last installment
//Let's cleanup for last time to focus on 6502
void Substract()
//subtract the value in Accumulator from the value on the stack
//Accumulator = Stack - Accumulator
//leave difference in Accumulator
{
    Match('-');
    Term();

    //first store accumulator in zero page byte
    EmitLn("STA $00");
    //pop from stack
    EmitLn("PLA");
    //set carry bit which works as inverse borrow bit
    //if you don't set it, your answer will have extra bit subtracted
    //effectively Accum = Accum - memory - ~carry
    EmitLn("SEC");
    //Subtract what was in A from what was on stack
    EmitLn("SBC $00");
    //difference is left in Accumulator, carry bit clear if borrow occured
}

//new function for this installment
//changes scope to assume expression is always getting assigned to a variable
/* Jack slurps up variable name and equal sign
 * Then solves expression that follows the equal sign
 * And assigns the result of the expression (which was left in D0)
 * to the variable provided.
{--------------------------------------------------------------}
{ Parse and Translate an Assignment Statement }

procedure Assignment;
var Name: string[8];
begin
   Name := GetName;
   Match('=');
   Expression;
   EmitLn('LEA ' + Name + '(PC),A0');
   EmitLn('MOVE D0,(A0)')
end;
*/
void Assignment()
{
	//This code was broken because GetName sets the global token buffer char array to the string that's getting read
	//but GetName is used again elsewhere to collect other names, can't just set a pointer to token buffer and assume it 
	//never changes!!!
	//We need a new string variable and need to copy the GetName string into our local variable here so we can reuse it later
	//and it won't get stomped over the next time GetName utilizes token buffer!!
    int i = 0;
    //We'll just follow in Jack's footsteps for simplicity
    char var[8];
    char *name = GetName();
    //now copy string pointed to by name into our var string
    for( i=0; i<8; i++) {
	    var[i] = *name;
	    name++;
    }
    Match('=');
    Expression();
    //x86: sprintf(tmp, "lea %s, %%ebx", name);
    sprintf(tmp, "STA %s", var);
    EmitLn(tmp);
    //x86: EmitLn("movl %eax, (%ebx)");
}

int main()
{

    Init();
    /*x86 assembly file header
    EmitLn(".text");
    EmitLn(".global _start");
    EmitLn("_start:");
    */
    /* Expression(); */
    Assignment();
    if (Look != '\n') {
        Expected("NewLine");
    }


    /* return the result */
    /*x86 assembly file footer
    EmitLn("movl %eax, %ebx");
    EmitLn("movl $1, %eax");
    EmitLn("int $0x80");
    */
    return 0;
}
