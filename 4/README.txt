How to use this program:
call main.exe
Then you have 3 options
don't use any spaces
pick a variable A-Z and assign it to something like so:
A=5+1
now you can get output of A with ! as so
!A
press enter and you'll get the reply
	6
You can input variables without assignment using ?
?B8
assigns B to a value of 8.
now you can do something like:
D=B+A
!D
	14
Wow!  when you're done being impressed, use period to terminate
.

Because this is an interpreter alone, it doesn't compile any code.
So there's nothing to translate to 6502.  That was easy!
