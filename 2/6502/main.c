#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cradle.h"

void Term();
void Expression();
void Add();
void Substract();
void Factor();

/* Jack pops from stack and multiplies D0 leaving sum in D0
{--------------------------------------------------------------}
{ Recognize and Translate a Multiply }

procedure Multiply;
begin
   Match('*');
   Factor;
   EmitLn('MULS (SP)+,D0');  Source * Dest => Dest
   end;
 */
void Multiply()
{
    Match('*');
    Factor();
//Multiply next value on stack with reg, result in reg
//Pretty damn hard to do generically without hardware multiplier...
//Could pretend we have a hardware multiplier like NES MMC5
//Most legit means is a lookup table
//
//There are some example routines online such as follows:
//
//; General 8bit * 8bit = 8bit multiply
//; Multiplies "num1" by "num2" and returns result in .A
//
//; by White Flame (aka David Holz) 20030207
//
//; Input variables:
//;   num1 (multiplicand)
//;   num2 (multiplier), should be small for speed
//;   Signedness should not matter
//
//; .X and .Y are preserved
//; num1 and num2 get clobbered
//
//; Instead of using a bit counter, this routine ends when num2 reaches zero, thus saving iterations.
//
// lda #$00
// beq enterLoop
//
// doAdd:
// clc
// adc num1
//
// loop:
// asl num1
// enterLoop: ;For an accumulating multiply (.A = .A + num1*num2), set up num1 and num2, then enter here
// lsr num2
// bcs doAdd
// bne loop
//
// end:
//
// ; 15 bytes
//
// Perhaps the most generic means is to reserve two addresses/registers to store the operands
// Then jsr to some routine that'll handle the multiplication retruning with sum in accumulator

    //multiplicand is in Accumulator
    EmitLn("STA multiplicand");
    EmitLn("PLA");
    EmitLn("STA multiplier");
    EmitLn("JSR multiply");
    //Sum is in Accumlator upon return

    //x86:
    //EmitLn("imull (%esp), %eax");
    /* push of the stack */
    //EmitLn("addl $4, %esp");
} 

/* Jack pops from stack and stores denominator in D1, then divides D0 by D1 leaving quotient in D0
{-------------------------------------------------------------}
{ Recognize and Translate a Divide }

procedure Divide;
begin
   Match('/');
   Factor;
   EmitLn('MOVE (SP)+,D1');
   EmitLn('DIVS D1,D0'); Dest(D0) / Source(D1) => Dest(D0)
   end;
   */
void Divide()
{
    Match('/');
    Factor();

    //Similar to multiply, for the 6502, we'll store numerator and denominator
    //in memory/registers and then call a subroutine to perform division which
    //retruns with quotient in Accumulator, and maybe a remainder somewhere..?
    EmitLn("STA denominator");
    EmitLn("PLA");
    EmitLn("STA numerator");
    EmitLn("JSR divide");
    //quotient is in the Accumulator

    /* x86 for a expersion like a/b we have eax=b and %(esp)=a
     * but we need eax=a, and b on the stack 
    EmitLn("movl (%esp), %edx");
    EmitLn("addl $4, %esp");

    EmitLn("pushl %eax");

    EmitLn("movl %edx, %eax");

    sign extesnion 
    EmitLn("sarl $31, %edx");
    EmitLn("idivl (%esp)");
    EmitLn("addl $4, %esp");
     */

}

/* Jack handles parenths as their own expression,
 * else gets number and stores in D0
{---------------------------------------------------------------}
{ Parse and Translate a Math Factor }

procedure Expression; Forward;

procedure Factor;
begin
   if Look = '(' then begin
      Match('(');
      Expression;
      Match(')');
      end
   else
      EmitLn('MOVE #' + GetNum + ',D0');
end;
*/
void Factor()
{

    if(Look == '(') {

        Match('(');
        Expression();
        Match(')');
	/* catching - sign in term
     } else if(IsAddop(Look)) {

        Match('-');
        sprintf(tmp,"movl $%c, %%eax", GetNum());
        EmitLn(tmp);
        EmitLn("negl %eax");
	*/
    } else {

        //x86: sprintf(tmp,"movl $%c, %%eax", GetNum());
        sprintf(tmp,"LDA #$%c", GetNum());
        EmitLn(tmp);
    }
}

/* Jack calls factor, then handles mult/div by:
 * first pushing D0 to stack, then calling mult/div
{---------------------------------------------------------------}
{ Parse and Translate a Math Term }

procedure Term;
begin
   Factor;
   while Look in ['*', '/'] do begin
      EmitLn('MOVE D0,-(SP)');
      case Look of
       '*': Multiply;
       '/': Divide;
      else Expected('Mulop');
      end;
   end;
end;
*/
void Term()
{
    Factor();
    while (strchr("*/", Look)) {

        //x86: EmitLn("pushl %eax");
        EmitLn("PHA");

        switch(Look)
        {
            case '*':
                Multiply();
                break;
            case '/':
                Divide();
                break;
            default:
                Expected("Mulop");
        }
    }
}

/*
Jack starts by zeroing D0 if starts with +/-, else calls term
then handles +/- by first pushing D0 to stack then calling add/sub
{---------------------------------------------------------------}
{ Parse and Translate an Expression }

procedure Expression;
begin
   if IsAddop(Look) then
      EmitLn('CLR D0')
   else
      Term;
   while IsAddop(Look) do begin
      EmitLn('MOVE D0,-(SP)');
      case Look of
       '+': Add;
       '-': Subtract;
      else Expected('Addop');
      end;
   end;
end;
{--------------------------------------------------------------}
*/
void Expression()
{
    if(IsAddop(Look))	
    //starts with +/- sign
        //x86: EmitLn("xor %eax, %eax");
        EmitLn("LDA #$00");
    else
    //starts with a term
        Term();

    //Add/Sub op
    while (strchr("+-", Look)) {

	//push current term on stack
        //x86: EmitLn("pushl %eax");
        EmitLn("PHA");

        switch(Look)
        {
            case '+':
                Add();
                break;
            case '-':
                Substract();
                break;
            default:
                Expected("Addop");
        }
    }
}


/*
Jack calls term, then pops value from stack and adds to D0
Leaving sum in D0
{--------------------------------------------------------------}
{ Recognize and Translate an Add }

procedure Add;
begin
   Match('+');
   Term;
   EmitLn('ADD (SP)+,D0'); Source + Dest => Dest
end;
*/
void Add()
{
    Match('+');
    Term();
    //6502 can only perform addition with accumulator and memory/regs
    //we'll consider zero page as registers which can be added to accumulator
    //first store accumulator to a byte in zero page
    EmitLn("STA $00");
    //pop from stack to accum
    EmitLn("PLA");
    //Add Accumulator and ZPage byte
    EmitLn("ADC $00");
    //Sum is in accumulator

    /* x86
    EmitLn("addl (%esp), %eax");
    EmitLn("addl $4, %esp");
    */
    
}

/*
Jack calls term then subtracts value on stack from D0 
leaving difference in D0, but sign is wrong, so needs
to subtract difference from zero leaving difference in D0

Easier route would have been to subtract D0 from value on stack
if we're able to simply do that instead
{-------------------------------------------------------------}
{ Recognize and Translate a Subtract }

procedure Subtract;
begin
   Match('-');
   Term;
   EmitLn('SUB (SP)+,D0'); Dest - StackPop => Dest
   EmitLn('NEG D0');  0 - Dest => Dest
end;
*/
void Substract()
{
    Match('-');
    Term();
    //6502 can subtract values from memory/registers from the accumulator
    //The ultimate goal is to subtract the Accumulator from the value on the stack
    //This is actually ideal for the 6502 which is good cause we don't have a negate inst
    //first store accumulator in zero page byte
    EmitLn("STA $00");
    //pop from stack
    EmitLn("PLA");
    //Subtract what was in A from what was on stack
    EmitLn("SBC $00");
    //difference is left in Accumulator


    /* x86
    EmitLn("subl (%esp), %eax");
    EmitLn("negl %eax");
    EmitLn("addl $4, %esp");
    */
}


int main()
{
    Init();
    Expression();
    return 0;
}
